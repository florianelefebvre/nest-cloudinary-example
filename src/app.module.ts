import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CloudinaryModule } from './cloudinary/cloudinary.module';
import { Cloudinary } from './cloudinary';

@Module({
  imports: [CloudinaryModule],
  controllers: [AppController],
  providers: [AppService, Cloudinary],
})
export class AppModule {}
