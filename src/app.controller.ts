import { Post } from '@nestjs/common';
import { UseInterceptors } from '@nestjs/common';
import { Body } from '@nestjs/common';
import { UploadedFile } from '@nestjs/common';
import { BadRequestException } from '@nestjs/common';
import { Controller, Get } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { AppService } from './app.service';
import { Cloudinary } from './cloudinary';
import { CloudinaryService } from './cloudinary/cloudinary.service';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private cloudinaryService: CloudinaryService,
  ) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  // TESTS
  @Post('upload')
  @UseInterceptors(FileInterceptor('profile_picture'))
  async uploadFImageToCloudinary(@UploadedFile() file: Express.Multer.File) {
    console.log('file : ', file);

    try {
      const response = await this.cloudinaryService.uploadImage(file);
      console.log('response in cloudinary', response);
    } catch (error) {
      throw new BadRequestException('Invalid file type.');
    }
  }
}
